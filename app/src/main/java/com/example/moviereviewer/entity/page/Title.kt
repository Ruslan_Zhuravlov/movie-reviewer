package com.example.moviereviewer.entity.page

import androidx.annotation.StringRes
import com.example.moviereviewer.R

enum class Title(@StringRes val resId: Int) {

    FILMS(R.string.films),
    FAVOURITES(R.string.favourites),
    DETAILS(R.string.details)
}