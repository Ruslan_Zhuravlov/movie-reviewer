package com.example.moviereviewer.entity.film

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Film(
    val id: Int,
    val name: String,
    val releaseDate: String,
    val originalLanguage: String,
    val description: String,
    val isAdult: Boolean,
    val popularityRate: Double,
    val voteAverage: Double,
    val voteCount: Int,
    val posterPath: String
) : Parcelable
