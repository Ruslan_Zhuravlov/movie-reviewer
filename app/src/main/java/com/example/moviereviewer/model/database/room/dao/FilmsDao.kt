package com.example.moviereviewer.model.database.room.dao

import androidx.room.*
import com.example.moviereviewer.model.database.room.entity.DbFilm

@Dao
interface FilmsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertFilm(dbSetting: DbFilm)

    @Delete
    fun deleteFilm(dbSetting: DbFilm)

    @Query("SELECT * FROM films")
    fun queryAllFilms(): List<DbFilm>
}