package com.example.moviereviewer.model.database.room.mapper

import com.example.moviereviewer.entity.film.Film
import com.example.moviereviewer.model.database.room.entity.DbFilm

object DBMapper {

    fun from(film: Film) = DbFilm(
        id = film.id,
        name = film.name,
        releaseDate = film.releaseDate,
        originalLanguage = film.originalLanguage,
        description = film.description,
        isAdult = film.isAdult,
        popularityRate = film.popularityRate,
        voteAverage = film.voteAverage,
        voteCount = film.voteCount,
        posterPath = film.posterPath
    )

    fun from(dbFilm: DbFilm) = Film(
        id = dbFilm.id,
        name = dbFilm.name,
        releaseDate = dbFilm.releaseDate,
        originalLanguage = dbFilm.originalLanguage,
        description = dbFilm.description,
        isAdult = dbFilm.isAdult,
        popularityRate = dbFilm.popularityRate,
        voteAverage = dbFilm.voteAverage,
        voteCount = dbFilm.voteCount,
        posterPath = dbFilm.posterPath
    )
}