package com.example.moviereviewer.model.network.entity

import com.google.gson.annotations.SerializedName

data class PopularFilmsResponse(
    @SerializedName("page")
    val page: Int,
    @SerializedName("results")
    val results: List<FilmResponse>
)