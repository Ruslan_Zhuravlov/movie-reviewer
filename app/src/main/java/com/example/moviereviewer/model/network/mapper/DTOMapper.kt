package com.example.moviereviewer.model.network.mapper

import com.example.moviereviewer.entity.film.Film
import com.example.moviereviewer.model.network.entity.FilmResponse

object DTOMapper {

    fun toFilm(filmResponse: FilmResponse) =
        Film(
            id = filmResponse.id,
            name = filmResponse.original_title,
            releaseDate = filmResponse.release_date,
            originalLanguage = filmResponse.original_language,
            description = filmResponse.overview,
            isAdult = filmResponse.adult,
            popularityRate = filmResponse.popularity,
            voteAverage = filmResponse.vote_average,
            voteCount = filmResponse.vote_count,
            posterPath = filmResponse.poster_path
        )
}