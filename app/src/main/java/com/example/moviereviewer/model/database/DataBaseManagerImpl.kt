package com.example.moviereviewer.model.database

import android.content.Context
import androidx.room.Room
import com.example.moviereviewer.entity.film.Film
import com.example.moviereviewer.model.database.room.db.AppDataBase
import com.example.moviereviewer.model.database.room.mapper.DBMapper

class DataBaseManagerImpl(private val context: Context) : DataBaseManager {

    private val database: AppDataBase by lazy {
        Room.databaseBuilder(
            context.applicationContext,
            AppDataBase::class.java, DATABASE_NAME
        ).build()
    }

    override suspend fun addFilm(film: Film) {
        database.filmsDao().insertFilm(DBMapper.from(film))
    }

    override suspend fun deleteFilm(film: Film) {
        database.filmsDao().deleteFilm(DBMapper.from(film))
    }

    override suspend fun getAllFilms() =
        database.filmsDao().queryAllFilms().map { dbFilm -> DBMapper.from(dbFilm) }

    companion object {
        private const val DATABASE_NAME = "movie_reviewer.db"
    }
}