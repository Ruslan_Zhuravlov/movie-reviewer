package com.example.moviereviewer.model.network

import com.example.moviereviewer.model.network.api.APINetwork
import com.example.moviereviewer.model.network.mapper.DTOMapper
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class NetworkManagerImpl : NetworkManager {

    private val adapterFactory: CoroutineCallAdapterFactory = CoroutineCallAdapterFactory()
    private val retrofit: Retrofit by lazy {
        Retrofit.Builder()
            .baseUrl(TMDB_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(adapterFactory)
            .build()
    }
    private val api: APINetwork by lazy { retrofit.create(APINetwork::class.java) }

    override suspend fun getPopularFilms(apiKey: String?) =
        api.getPopularFilms(apiKey ?: TMDB_API_KEY).results.map { DTOMapper.toFilm(it) }

    companion object {
        private const val TMDB_BASE_URL = "https://api.themoviedb.org/"
        private const val TMDB_API_KEY = "b2e5e828e613acd2e52d5e418ba740b8"
    }
}