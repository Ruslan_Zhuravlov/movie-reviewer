package com.example.moviereviewer.model.database.room.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.moviereviewer.BuildConfig
import com.example.moviereviewer.model.database.room.dao.FilmsDao
import com.example.moviereviewer.model.database.room.entity.DbFilm

@Database(entities = [DbFilm::class], version = BuildConfig.DATABASE_VERSION, exportSchema = false)

abstract class AppDataBase : RoomDatabase() {

    abstract fun filmsDao(): FilmsDao
}