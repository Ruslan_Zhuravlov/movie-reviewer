package com.example.moviereviewer.model.database

import com.example.moviereviewer.entity.film.Film

interface DataBaseManager {

    suspend fun addFilm(film: Film)

    suspend fun deleteFilm(film: Film)

    suspend fun getAllFilms(): List<Film>
}