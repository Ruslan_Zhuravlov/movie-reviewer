package com.example.moviereviewer.model.network

import com.example.moviereviewer.entity.film.Film

interface NetworkManager {

    suspend fun getPopularFilms(apiKey: String? = null): List<Film>
}