package com.example.moviereviewer.model.network.api

import com.example.moviereviewer.model.network.entity.PopularFilmsResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface APINetwork {

    @GET("3/discover/movie")
    suspend fun getPopularFilms(
        @Query("api_key") apiKey: String,
        @Query("sort_by") sort_by: String = "popularity.desc",
    ): PopularFilmsResponse
}