package com.example.moviereviewer.repository.film

import com.example.moviereviewer.entity.film.Film

interface FilmRepository {

    suspend fun getPopularFilms(): List<Film>

    suspend fun addFilm(film: Film)

    suspend fun removeFilm(film: Film)

    suspend fun getFavouriteFilms(): List<Film>
}