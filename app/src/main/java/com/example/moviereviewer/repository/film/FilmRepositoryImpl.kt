package com.example.moviereviewer.repository.film

import com.example.moviereviewer.entity.film.Film
import com.example.moviereviewer.model.database.DataBaseManager
import com.example.moviereviewer.model.network.NetworkManager

class FilmRepositoryImpl(
    private val dataBaseManager: DataBaseManager,
    private val networkManager: NetworkManager,
) : FilmRepository {

    override suspend fun getPopularFilms() = networkManager.getPopularFilms()

    override suspend fun addFilm(film: Film) = dataBaseManager.addFilm(film)

    override suspend fun removeFilm(film: Film) = dataBaseManager.deleteFilm(film)

    override suspend fun getFavouriteFilms() = dataBaseManager.getAllFilms()
}