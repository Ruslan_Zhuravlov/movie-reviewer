package com.example.moviereviewer.core.base.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.moviereviewer.R
import com.example.moviereviewer.utils.log.Logger
import kotlinx.coroutines.*

abstract class BaseFragment : Fragment(), CoroutineScope {

    private val job: Job = SupervisorJob()
    private val exceptionHandler = CoroutineExceptionHandler { _, exception ->
        onError(exception)
    }

    final override val coroutineContext = Dispatchers.Main + job + exceptionHandler

    @LayoutRes
    abstract fun getLayoutResId(): Int

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(getLayoutResId(), container, false)

    open fun onError(throwable: Throwable) {
        Logger.ml(
            info = "BaseFragment onError()",
            location = "${this::class}",
            exception = throwable,
            showStackTrace = true
        )
    }

    fun toast(message: String?) {
        message?.let { Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show() }
    }

    fun toast(resId: Int?) {
        resId?.let { textResId ->
            val text = try {
                requireContext().getString(textResId)
            } catch (exception: Throwable) {
                requireContext().getString(R.string.unknown_message)
            }
            Toast.makeText(requireContext(), text, Toast.LENGTH_SHORT).show()
        }
    }

    inline fun <A : RecyclerView.Adapter<*>, T> updateDiff(
        adapter: A,
        oldList: MutableList<T>,
        newList: List<T>,
        crossinline compareItemsAreTheSame: (Pair<T, T>) -> Boolean,
        detectMoves: Boolean = true
    ) = launch(Dispatchers.IO) {
        val diffResult = DiffUtil.calculateDiff(
            object : DiffUtil.Callback() {
                override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
                    compareItemsAreTheSame(oldList[oldItemPosition] to newList[newItemPosition])

                override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) =
                    oldList[oldItemPosition] == newList[newItemPosition]

                override fun getOldListSize() = oldList.size

                override fun getNewListSize() = newList.size
            },
            detectMoves
        )
        withContext(Dispatchers.Main) {
            oldList.clear()
            oldList.addAll(newList)
            diffResult.dispatchUpdatesTo(adapter)
        }
    }
}