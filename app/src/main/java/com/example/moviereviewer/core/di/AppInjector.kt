package com.example.moviereviewer.core.di

import android.app.Application
import com.example.moviereviewer.feature.MainViewModel
import com.example.moviereviewer.feature.fragment.favourites.FavouritesViewModel
import com.example.moviereviewer.feature.fragment.films.FilmsViewModel
import com.example.moviereviewer.model.database.DataBaseManager
import com.example.moviereviewer.model.database.DataBaseManagerImpl
import com.example.moviereviewer.model.network.NetworkManager
import com.example.moviereviewer.model.network.NetworkManagerImpl
import com.example.moviereviewer.repository.film.FilmRepository
import com.example.moviereviewer.repository.film.FilmRepositoryImpl
import org.koin.core.context.startKoin
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

object AppInjector {

    fun initModules(context: Application) {
        startKoin {
            androidContext(context)
            modules(viewModelsModule, managersModule, repositoriesModule)
        }
    }

    private val managersModule = module {
        single<DataBaseManager> { DataBaseManagerImpl(androidContext()) }
        single<NetworkManager> { NetworkManagerImpl() }
    }

    private val repositoriesModule = module {
        factory<FilmRepository> { FilmRepositoryImpl(get(), get()) }
    }

    private val viewModelsModule = module {
        viewModel { FavouritesViewModel(get()) }
        viewModel { FilmsViewModel(get()) }
        viewModel { MainViewModel() }
    }
}