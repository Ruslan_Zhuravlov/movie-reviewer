package com.example.moviereviewer.core.base.viewmodel

import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ProcessLifecycleOwner
import androidx.lifecycle.ViewModel
import com.example.moviereviewer.utils.log.Logger
import kotlinx.coroutines.*

abstract class BaseViewModel : ViewModel(), CoroutineScope, LifecycleObserver {

    val progressBarLD: MutableLiveData<Boolean> = MutableLiveData()
    val toastLD: MutableLiveData<String> = MutableLiveData()
    val toastResLD: MutableLiveData<Int> = MutableLiveData()

    private val job: Job = SupervisorJob()
    private val exceptionHandler = CoroutineExceptionHandler { _, exception -> onError(exception) }

    final override val coroutineContext = Dispatchers.Main + job + exceptionHandler

    override fun onCleared() {
        ProcessLifecycleOwner.get().lifecycle.removeObserver(this)
        coroutineContext.cancelChildren()
        super.onCleared()
    }

    open fun onError(exception: Throwable) = launch {
        Logger.ml(
            info = "BaseViewModel onError()",
            location = "${this::class}",
            exception = exception,
            showStackTrace = true
        )
        hideProgress()
    }

    open fun showProgress() {
        progressBarLD.value = true
    }

    open fun hideProgress() {
        progressBarLD.value = false
    }
}