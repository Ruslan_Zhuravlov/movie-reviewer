package com.example.moviereviewer.core.base.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class BaseAdapter<T, VH : BaseAdapter.BaseViewHolder<T>>(
    open val items: MutableList<T> = mutableListOf()
) : RecyclerView.Adapter<VH>() {

    override fun getItemCount() = items.size

    fun isInPositionsRange(position: Int) = position != RecyclerView.NO_POSITION && position < itemCount

    abstract class BaseViewHolder<T>(view: View) : RecyclerView.ViewHolder(view) {

        abstract fun onBind(item: T)
    }
}