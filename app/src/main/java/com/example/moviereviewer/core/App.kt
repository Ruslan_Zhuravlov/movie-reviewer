package com.example.moviereviewer.core

import android.app.Application
import com.example.moviereviewer.core.config.Configuration
import com.example.moviereviewer.core.di.AppInjector

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        AppInjector.initModules(this)
        Configuration.setup()
    }
}