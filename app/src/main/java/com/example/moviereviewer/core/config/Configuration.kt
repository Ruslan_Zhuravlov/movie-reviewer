package com.example.moviereviewer.core.config

import com.example.moviereviewer.BuildConfig
import timber.log.Timber

object Configuration {

    fun setup() {
        initTimber()
    }

    private fun initTimber() {
        if (BuildConfig.DEBUG) Timber.plant(Timber.DebugTree())
    }
}