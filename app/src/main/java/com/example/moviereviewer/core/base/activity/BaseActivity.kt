package com.example.moviereviewer.core.base.activity

import android.os.Bundle
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import com.example.moviereviewer.utils.log.Logger
import kotlinx.coroutines.*

abstract class BaseActivity : AppCompatActivity(), CoroutineScope {

    private val job: Job = SupervisorJob()
    private val exceptionHandler = CoroutineExceptionHandler { _, exception ->
        onError(exception)
    }

    final override val coroutineContext = Dispatchers.Main + job + exceptionHandler

    @LayoutRes
    abstract fun getLayoutResID(): Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutResID())
    }

    open fun onError(throwable: Throwable) {
        Logger.ml(
            info = "BaseActivity onError()",
            location = "${this::class}",
            exception = throwable,
            showStackTrace = true
        )
    }

    fun toast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }
}