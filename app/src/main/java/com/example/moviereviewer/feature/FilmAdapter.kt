package com.example.moviereviewer.feature

import android.view.View
import android.view.ViewGroup
import com.example.moviereviewer.R
import com.example.moviereviewer.core.base.adapter.BaseAdapter
import com.example.moviereviewer.entity.film.Film
import com.example.moviereviewer.utils.extension.inflate
import com.example.moviereviewer.utils.extension.loadFromUrl
import kotlinx.android.synthetic.main.item_film.view.*

class FilmAdapter(
    override val items: MutableList<Film> = mutableListOf()
) : BaseAdapter<Film, FilmAdapter.FilmViewHolder>() {

    var showDetailsAction: ((Film) -> Unit)? = null
    var addToFavouritesAction: ((Film) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        FilmViewHolder(parent.inflate(R.layout.item_film))

    override fun onBindViewHolder(holder: FilmViewHolder, position: Int) {
        if (!isInPositionsRange(position)) return
        with(holder.itemView) {
            holder.onBind(items[position])
            btnFilmDetails.setOnClickListener {
                if (isInPositionsRange(position)) showDetailsAction?.invoke(items[position])
            }
            btnFilmFavourites.setOnClickListener {
                if (isInPositionsRange(position)) addToFavouritesAction?.invoke(items[position])
            }
        }
    }

    class FilmViewHolder(view: View) : BaseAdapter.BaseViewHolder<Film>(view) {

        override fun onBind(item: Film) {
            with(itemView) {
                tvFilmName.text = item.name
                val url = "$BASE_URL${item.posterPath}"
                ivFilmPoster.loadFromUrl(url)
            }
        }
    }

    companion object {
        private const val BASE_URL = "https://image.tmdb.org/t/p/w440_and_h660_face"
    }
}