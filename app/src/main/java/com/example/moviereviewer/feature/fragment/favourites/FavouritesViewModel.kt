package com.example.moviereviewer.feature.fragment.favourites

import androidx.lifecycle.MutableLiveData
import com.example.moviereviewer.R
import com.example.moviereviewer.core.base.viewmodel.BaseViewModel
import com.example.moviereviewer.entity.film.Film
import com.example.moviereviewer.repository.film.FilmRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class FavouritesViewModel(
    private val filmRepository: FilmRepository
) : BaseViewModel() {

    val filmsLD: MutableLiveData<List<Film>> = MutableLiveData()

    fun requestFavouriteFilms() = launch {
        try {
            showProgress()
            val films = withContext(Dispatchers.IO) { filmRepository.getFavouriteFilms() }
            filmsLD.value = films
        } catch (exception: Throwable) {
            onError(exception)
            toastLD.value = exception.message
            toastLD.value = null
        } finally {
            hideProgress()
        }
    }

    fun removeFromFavourites(film: Film) = launch {
        try {
            showProgress()
            withContext(Dispatchers.IO) { filmRepository.removeFilm(film) }
            toastResLD.value = R.string.removed
            toastResLD.value = null
            requestFavouriteFilms()
        } catch (exception: Throwable) {
            onError(exception)
            toastLD.value = exception.message
            toastLD.value = null
        } finally {
            hideProgress()
        }
    }
}