package com.example.moviereviewer.feature

import androidx.lifecycle.MutableLiveData
import com.example.moviereviewer.core.base.viewmodel.BaseViewModel
import com.example.moviereviewer.entity.page.Title

class MainViewModel : BaseViewModel() {

    val favoritesLD: MutableLiveData<Unit> = MutableLiveData()
    val titleLD: MutableLiveData<Int> = MutableLiveData()

    fun navigateToFavorites() {
        favoritesLD.value = Unit
        favoritesLD.value = null
    }

    fun setTitle(title: Title) {
        titleLD.value = title.resId
    }
}