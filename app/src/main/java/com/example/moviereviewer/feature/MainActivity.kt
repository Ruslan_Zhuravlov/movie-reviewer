package com.example.moviereviewer.feature

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.example.moviereviewer.R
import com.example.moviereviewer.core.base.activity.BaseActivity
import com.example.moviereviewer.utils.extension.gone
import com.example.moviereviewer.utils.extension.observe
import com.example.moviereviewer.utils.extension.visible
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : BaseActivity() {

    private val viewModel: MainViewModel by viewModel()

    override fun getLayoutResID() = R.layout.activity_main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setSupportActionBar(tbMain)
        with(viewModel) {
            observe(progressBarLD) { showProgress ->
                if (showProgress) clProgress.visible() else clProgress.gone()
            }
            observe(titleLD) { resId ->
                supportActionBar?.title = getString(resId)
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) =
        when (item.itemId) {
            R.id.actionFavorites -> {
                viewModel.navigateToFavorites()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
}