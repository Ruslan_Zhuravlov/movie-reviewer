package com.example.moviereviewer.feature.fragment.detail

import android.os.Bundle
import android.view.View
import com.example.moviereviewer.R
import com.example.moviereviewer.core.base.fragment.BaseFragment
import com.example.moviereviewer.entity.page.Title.DETAILS
import com.example.moviereviewer.feature.MainViewModel
import com.example.moviereviewer.utils.extension.filmFromArgs
import kotlinx.android.synthetic.main.fragment_detail.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class DetailFragment : BaseFragment() {

    private val sharedViewModel: MainViewModel by sharedViewModel()

    override fun getLayoutResId() = R.layout.fragment_detail

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(sharedViewModel) {
            setTitle(DETAILS)
        }
        val film = filmFromArgs()
        film?.let { _film ->
            tvFilmDetailId.text = _film.id.toString()
            tvFilmDetailName.text = _film.name
            tvFilmDetailReleaseDate.text = _film.releaseDate
            tvFilmDetailOriginalLanguage.text = _film.originalLanguage
            tvFilmDetailDescription.text = _film.description
            tvFilmDetailIsAdult.text = _film.isAdult.toString()
            tvFilmDetailPopularityRate.text = _film.popularityRate.toString()
            tvFilmDetailVoteAverage.text = _film.voteAverage.toString()
            tvFilmDetailVoteCount.text = _film.voteCount.toString()
        }
    }
}