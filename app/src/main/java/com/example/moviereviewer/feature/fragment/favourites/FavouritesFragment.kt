package com.example.moviereviewer.feature.fragment.favourites

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import com.example.moviereviewer.R
import com.example.moviereviewer.core.base.fragment.BaseFragment
import com.example.moviereviewer.entity.page.Title.FAVOURITES
import com.example.moviereviewer.feature.FilmAdapter
import com.example.moviereviewer.feature.MainViewModel
import com.example.moviereviewer.utils.extension.filmToBundle
import com.example.moviereviewer.utils.extension.observe
import kotlinx.android.synthetic.main.fragment_favourites.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class FavouritesFragment : BaseFragment() {

    private val sharedViewModel: MainViewModel by sharedViewModel()
    private val viewModel: FavouritesViewModel by viewModel()

    override fun getLayoutResId() = R.layout.fragment_favourites

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val favoriteAdapter = FilmAdapter().apply {
            rvFavouriteFilms.adapter = this
            showDetailsAction = { film ->
                findNavController().navigate(R.id.detailFragment, filmToBundle(film))
            }
            addToFavouritesAction = { film ->
                viewModel.removeFromFavourites(film)
            }
        }
        with(sharedViewModel) {
            setTitle(FAVOURITES)
        }
        with(viewModel) {
            observe(progressBarLD) { showProgress ->
                if (showProgress) sharedViewModel.showProgress() else sharedViewModel.hideProgress()
            }
            observe(toastLD) { text -> toast(text) }
            observe(toastResLD) { resId -> toast(resId) }
            observe(filmsLD) { items ->
                updateDiff(
                    adapter = favoriteAdapter,
                    oldList = favoriteAdapter.items,
                    newList = items,
                    compareItemsAreTheSame = { (first, second) -> first.id == second.id },
                    detectMoves = true
                )
            }
            requestFavouriteFilms()
        }
    }
}