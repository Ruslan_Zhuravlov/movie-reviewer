package com.example.moviereviewer.feature.fragment.films

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import com.example.moviereviewer.R
import com.example.moviereviewer.core.base.fragment.BaseFragment
import com.example.moviereviewer.entity.page.Title.FILMS
import com.example.moviereviewer.feature.FilmAdapter
import com.example.moviereviewer.feature.MainViewModel
import com.example.moviereviewer.utils.extension.filmToBundle
import com.example.moviereviewer.utils.extension.observe
import kotlinx.android.synthetic.main.fragment_films.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class FilmsFragment : BaseFragment() {

    private val sharedViewModel: MainViewModel by sharedViewModel()
    private val viewModel: FilmsViewModel by viewModel()

    override fun getLayoutResId() = R.layout.fragment_films

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val filmAdapter = FilmAdapter().apply {
            rvFilms.adapter = this
            showDetailsAction = { film ->
                findNavController().navigate(R.id.detailFragment, filmToBundle(film))
            }
            addToFavouritesAction = { film -> viewModel.addToFavourites(film) }
        }
        with(sharedViewModel) {
            setTitle(FILMS)
            observe(favoritesLD) { navigate ->
                navigate?.let { findNavController().navigate(R.id.favouritesFragment) }
            }
        }
        with(viewModel) {
            observe(progressBarLD) { showProgress ->
                if (showProgress) sharedViewModel.showProgress() else sharedViewModel.hideProgress()
            }
            observe(toastLD) { text -> toast(text) }
            observe(toastResLD) { resId -> toast(resId) }
            observe(filmsLD) { items ->
                updateDiff(
                    adapter = filmAdapter,
                    oldList = filmAdapter.items,
                    newList = items,
                    compareItemsAreTheSame = { (first, second) -> first.id == second.id },
                    detectMoves = true
                )
            }
            requestFilms()
        }
    }
}