package com.example.moviereviewer.feature.fragment.films

import androidx.lifecycle.MutableLiveData
import com.example.moviereviewer.R
import com.example.moviereviewer.core.base.viewmodel.BaseViewModel
import com.example.moviereviewer.entity.film.Film
import com.example.moviereviewer.repository.film.FilmRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class FilmsViewModel(
    private val filmRepository: FilmRepository
) : BaseViewModel() {

    val filmsLD: MutableLiveData<List<Film>> = MutableLiveData()

    fun requestFilms() = launch {
        try {
            showProgress()
            val films = withContext(Dispatchers.IO) { filmRepository.getPopularFilms() }
            filmsLD.value = films
        } catch (exception: Throwable) {
            onError(exception)
            toastLD.value = exception.message
            toastLD.value = null
        } finally {
            hideProgress()
        }
    }

    fun addToFavourites(film: Film) = launch {
        try {
            showProgress()
            withContext(Dispatchers.IO) { filmRepository.addFilm(film) }
            toastResLD.value = R.string.added
            toastResLD.value = null
        } catch (exception: Throwable) {
            onError(exception)
            toastLD.value = exception.message
            toastLD.value = null
        } finally {
            hideProgress()
        }
    }
}