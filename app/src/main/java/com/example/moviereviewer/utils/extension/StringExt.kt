package com.example.moviereviewer.utils.extension

import com.example.moviereviewer.BuildConfig.APPLICATION_ID as APP_ID

fun String.asBundleKey() = "$APP_ID$BUNDLE_KEY$this"

private const val BUNDLE_KEY = "_bundle_key_"
