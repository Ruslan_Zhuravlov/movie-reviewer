package com.example.moviereviewer.utils.extension

import androidx.annotation.DrawableRes
import androidx.appcompat.widget.AppCompatImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.moviereviewer.R

fun AppCompatImageView.loadFromUrl(
    url: String?,
    requestOptions: RequestOptions? = null,
    @DrawableRes
    placeholderImage: Int = R.drawable.ic_holder
) {
    Glide.with(context.applicationContext)
        .load(url)
        .placeholder(placeholderImage)
        .apply(requestOptions ?: RequestOptions().autoClone())
        .into(this)
}