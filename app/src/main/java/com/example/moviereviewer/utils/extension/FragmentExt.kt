package com.example.moviereviewer.utils.extension

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.moviereviewer.entity.film.Film

fun filmToBundle(film: Film): Bundle {
    val bundle = Bundle()
    bundle.putParcelable(FILM.asBundleKey(), film)
    return bundle
}

fun Fragment.filmFromArgs(): Film? = arguments?.getParcelable(FILM.asBundleKey())

private const val FILM = "film"