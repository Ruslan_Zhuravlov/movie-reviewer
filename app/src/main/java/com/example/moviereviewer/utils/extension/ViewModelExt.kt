package com.example.moviereviewer.utils.extension

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer

inline fun <reified T : Any, reified L : LiveData<T>> Fragment.observe(
    liveData: L,
    noinline body: (T) -> Unit
) = liveData.observe(this.viewLifecycleOwner, Observer(body))

inline fun <reified T : Any, reified L : LiveData<T>> AppCompatActivity.observe(
    liveData: L,
    noinline body: (T) -> Unit
) = liveData.observe(this, Observer(body))

inline fun <reified L : MutableLiveData<T>, T> L.invalidate() {
    this.value = this.value
}