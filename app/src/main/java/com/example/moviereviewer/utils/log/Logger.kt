package com.example.moviereviewer.utils.log

import timber.log.Timber
import java.io.PrintWriter
import java.io.StringWriter

object Logger {

    fun ml(
        info: String? = "",
        location: String? = "",
        exception: Throwable? = null,
        message: String? = exception?.message,
        showStackTrace: Boolean = false
    ) {
        if (exception != null) {
            exception.printStackTrace()
            val printStackTraceText = if (showStackTrace) {
                val stringWriter = StringWriter()
                exception.printStackTrace(writer = PrintWriter(stringWriter))
                "\nprintStackTrace=$stringWriter\n"
            } else ""
            Timber
                .tag("ml")
                .e("location: \n$location \ninfo=$info \nexception=$exception \nexception.message=$message $printStackTraceText")
        } else {
            Timber
                .tag("ml")
                .d("$info")
        }
    }

    fun wml(message: String) {
        Timber
            .tag("ml")
            .e(message)
    }
}