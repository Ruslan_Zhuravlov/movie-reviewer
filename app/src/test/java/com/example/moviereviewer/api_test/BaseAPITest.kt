package com.example.moviereviewer.api_test

import com.example.moviereviewer.model.network.NetworkManager
import com.example.moviereviewer.model.network.NetworkManagerImpl
import kotlinx.coroutines.*
import org.junit.After
import org.junit.Before
import org.mockito.Mockito.*

abstract class BaseAPITest : CoroutineScope {

    private val job: Job = SupervisorJob()

    override val coroutineContext = Dispatchers.IO + job

    lateinit var networkManager: NetworkManager

    @Before
    fun beforeTest() {
        print("\n\n")
        networkManager = spy(NetworkManagerImpl())
    }

    @After
    fun afterTest() {
        print("\n\n")
        coroutineContext.cancelChildren()
        reset(networkManager)
    }
}