package com.example.moviereviewer.api_test

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.junit.Assert
import org.junit.Test
import retrofit2.HttpException

class GetFilmsTest : BaseAPITest() {

    @Test
    fun getPopularFilmsSuccess() = runBlocking {
        print("expected = $FILMS_PER_PAGE\n")
        val response = withContext(Dispatchers.IO) { networkManager.getPopularFilms() }
        print("response = ${response.size}")
        Assert.assertEquals(FILMS_PER_PAGE, response.size)
    }

    @Test
    fun getPopularFilmsAuthError() = runBlocking {
        print("expected = $HTTP_EXCEPTION_UNAUTHORIZED_CODE\n")
        try {
            withContext(Dispatchers.IO) { networkManager.getPopularFilms(INVALID_API_KEY) }
            Assert.fail()
        } catch (exception: HttpException) {
            print("response = ${exception.code()}")
            Assert.assertEquals(HTTP_EXCEPTION_UNAUTHORIZED_CODE, exception.code())
        }
    }

    companion object {
        private const val INVALID_API_KEY = "b2e5e828e613acd2e52d5e418ba740b_"
        private const val FILMS_PER_PAGE = 20
        private const val HTTP_EXCEPTION_UNAUTHORIZED_CODE = 401
    }
}